#coding: utf-8
import os
import telebot
import settings

bot = telebot.TeleBot(settings.TOKEN)

FILEPATH = os.path.join(os.getcwd(), settings.FILENAME)

commands = """
/list - список покупок
/add or + - добавить пункт в список
/rm or - - удалить пункт из списка
"""

def _check_file_exists():
    if not os.path.isfile(FILEPATH):
        with open(settings.FILENAME, 'w+') as f:
            pass


@bot.message_handler(commands=['list'])
def handle_list(message):
    """
    :param message:
    :return:
    """
    msg = ''
    with open('list', 'r+') as f:
        lines = f.read()
        if lines:
            for i, rec in enumerate(lines.split(','), 1):
                msg += '%s. %s\n' % (i, rec)

    if not msg:
        msg = 'Список пуст'
    bot.send_message(message.chat.id, msg)


@bot.message_handler(commands=['add'])
def handle_add(message):
    """
    :param message:
    :return:
    """
    items = message.text.lstrip('/add ')
    items = items.strip()
    existed = []
    added = []
    msg = ''
    if not items:
        msg = 'Укажите покупку (e.g. /add хлеб)'
    else:
        with open(FILEPATH, 'r+') as f:
            file_list = f.read()
            if file_list:
                file_list = file_list.split(',')
            else:
                file_list = []

        for item in set(items.split(',')):
            found = False
            for rec in file_list:
                if item.strip().lower() == rec:
                    found = True
                    existed.append(rec)
                    break
            if not found:
                added.append(item)

        file_list = file_list + added

        with open(FILEPATH, 'w+') as f:
            f.write(','.join([rec.strip() for rec in file_list]))

        if added:
            msg = '"%s" добавлен(ы) в список покупок' % ', '.join(added)
            msg += '\n'
        if existed:
            msg += '"%s" уже есть в списке' % ', '.join(existed)

    bot.send_message(message.chat.id, msg)


@bot.message_handler(commands=['rm'])
def handle_remove(message):
    """
    :param message:
    :return:
    """
    deleted = []
    msg = ''
    items = message.text.lstrip('/rm ')
    try:
        items = set(int(i) for i in items.strip().split(','))
    except ValueError:
        return bot.send_message(
            message.chat.id, 'Необходимо указать числовые индексы')

    if not items:
        msg = 'Укажите номер покупки (e.g. /rm 4)'
    else:
        with open(FILEPATH, 'r+') as f:
            file_lines = f.read()

        if file_lines:
            file_lines = file_lines.split(',')
            for i, rec in enumerate(file_lines, 1):
                for item in items:
                    if item == i:
                        deleted.append(rec)

            output = [r for r in file_lines if r not in deleted]

            with open(FILEPATH, 'w+') as f:
                f.write(','.join(output))

            for i, rec in enumerate(output, 1):
                msg += '%s. %s\n' % (i, rec)
        else:
            msg = 'Список пуст'

    bot.send_message(message.chat.id, msg)


@bot.message_handler(content_types=["text"])
def base_answer(message):
    """
    :param message:
    :return:
    """
    #shortcuts
    if message.text.lower().startswith('-'):
        message.text = message.text.lstrip('-')
        return handle_remove(message)

    if message.text.lower().startswith('+'):
        message.text = message.text.lstrip('+')
        return handle_add(message)

    if message.text.lower().startswith('привет'):
        msg = 'Привет, Лиана'
    elif message.text.lower().startswith('как дела'):
        msg = 'У меня нет дел - я бездушный исполняемый код'
    else:
        msg = commands

    markup = telebot.types.ReplyKeyboardMarkup()
    list_button = telebot.types.KeyboardButton('/list')
    markup.row(list_button)

    bot.send_message(message.chat.id, msg, reply_markup=markup)


if __name__ == '__main__':
    _check_file_exists()
    bot.polling(none_stop=True)